/*
 * jQuery glisser v0.9
 * 
 * Copyright (c) 2013 Lukasz Kups
 * http://webrackets.com
 * http://lukaszkups.pl
 * @ofcapl
 *
 * Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
 */

 (function($, f){
  if(!$){ return f;}
  $.fn.glisser = function(options){
    options = $.extend({
      slideSpeed : 500,
      nextClass : '.next',
      prevClass : '.prev', 
      pagination : true,
      pauseAfterClick : true,
      paginationWidth : 50,
      paginationPositionX : 'center',
      paginationPositionY : 'bottom',
      autoplay : false,
      pauseDuration : 3000,
      looping : true,
      controlsType: 'normal',
      saveState: true,
      mainWrapper: $(this).selector
    }, options);

    return this.each(function(){
      var $this = $(this);
      var isAnimating = false;
      var interval;
      var elemList = [];
      var detachHelper;
      var slideDataHelper;
      var currentSlideDataHelper;
      var slideHelper;

      function returnWidth(){
        return parseInt($this.width(), 10);
      }
      function returnHeight(){
        return parseInt($this.height(), 10);
      }

      function returnSlideContainer(){
        return $this.find('ul.slide-container');
      }

      $this.find('li.slide').each(function(i){
        var listObj = {
          content: $(this).html(),
          nextElem: i+1,
          prevElem: i-1,
          pageId: i,
          classList: $(this).attr('class')
        };
        elemList.push(listObj);
        $(this).detach();
      });

      function returnCurrentMargin(){
        return parseInt($(this).css('margin-left'), 10); 
      }

      function pasteSlide(item){
        return "<li data-slide=\"" + item.pageId + "\" class=\"" + item.classList + "\" style=\"width:" +  returnWidth() + "px; height:" + returnHeight() + "px;\">" + item.content + "</li>";
      }

      function locateNextSlide(){
        return returnSlideContainer().find('li.slide').not('.current-slide'); 
      }

      function returnCurrentSlide(){
        return returnSlideContainer().find('li.current-slide');
      }

      function nextSlide(){
        return $(this).nextElem;
      }

      function prevSlide(){
        return $(this).prevElem;
      }

      function returnButtonWidth(){
        return parseInt($this.find(options.nextClass).width(), 10);
      }

      function returnButtonHeight(){
        return parseInt($this.find(options.nextClass).height(), 10);
      }

      function returnPaginationMargin(){
        return parseInt((returnWidth() - options.paginationWidth)/2, 10);
      }

      function setControls(){
        switch(options.controlsType){
          case 'big':
          $this.find(options.prevClass).css({'width':(returnWidth()/2) + 'px', 'height':returnHeight() + 'px', 'margin-top':''});
          $this.find(options.nextClass).css({'width':(returnWidth()/2) + 'px', 'height':returnHeight() + 'px', 'margin-left':(returnWidth()/2) + 'px', 'margin-top':''});
          break;

        case 'normal':
          $this.find(options.prevClass).css({'margin-top': ((returnHeight() - returnButtonHeight())/2) + 'px'});
          $this.find(options.nextClass).css({'margin-top': ((returnHeight() - returnButtonHeight())/2) + 'px', 'margin-left': (returnWidth() - returnButtonWidth()) + 'px'});
          returnSlideContainer().css({'margin-top' : '-' + returnButtonHeight() + 'px !important'});
          break;

        case 'above':
          $this.find(options.prevClass).css({'margin-top': 0 - returnButtonHeight()});
          $this.find(options.nextClass).css({'margin-top': 0 - returnButtonHeight(), 'margin-left': (returnWidth() - returnButtonWidth()) + 'px'});
          returnSlideContainer().css({'margin-top' : '-' + returnButtonHeight() + 'px !important'});
          break;

        case 'below':
          $this.find(options.prevClass).css({'margin-top': returnHeight() + 'px'});
          $this.find(options.nextClass).css({'margin-top': returnHeight() + 'px', 'margin-left': (returnWidth() - returnButtonWidth()) + 'px'});
          returnSlideContainer().css({'margin-top' : '-' + returnButtonHeight() + 'px !important'});
          break;

        case 'top':
          $this.find(options.prevClass).css({'margin-top': ''});
          $this.find(options.nextClass).css({'margin-top': '', 'margin-left': (returnWidth() - returnButtonWidth()) + 'px'});
          returnSlideContainer().css({'margin-top' : '-' + returnButtonHeight() + 'px !important'});
          break;

        /*case 'top-left':
          $this.find(options.prevClass).css({'margin-top': 0 - returnButtonHeight(), 'margin-left': });
          $this.find(options.nextClass).css({'margin-top': 0 - returnButtonHeight(), 'margin-left': (returnWidth() - returnButtonWidth()) + 'px'});
          break;*/

        case 'top-right':
          $this.find(options.prevClass).css({'margin-top': '-' + returnButtonHeight() + 'px', 'margin-left': (returnWidth() - returnButtonWidth()*2) + 'px'});
          $this.find(options.nextClass).css({'margin-top': '-' + returnButtonHeight() + 'px', 'margin-left': (returnWidth() - returnButtonWidth()) + 'px'});
          break;

        
        case 'for-forms':
          $this.find(options.prevClass).css({'display': 'none'});
          $this.find(options.nextClass).css({'margin-top': returnHeight() + 'px', 'margin-left': (returnWidth() - returnButtonWidth())/2 + 'px'});
          returnSlideContainer().css({'margin-top' : '-' + returnButtonHeight() + 'px !important'});
          options.looping = false;
          options.pagination = false;
          break;
        };

      }
      setControls();

      function updateCurrentSlide(){
        $('li.current-slide').css({'width':returnWidth() + 'px', 'height':returnHeight() + 'px'});
      }

      elemList[0].prevElem = elemList.length - 1;
      elemList[elemList.length - 1].nextElem = 0;
      
      var currentSlideHelper = elemList[0];

      returnSlideContainer().append(pasteSlide(elemList[0]));
      $this.find('li.slide').addClass('current-slide');

      function updatePagination(){
        if(options.pagination){
          $this.find('ul.pagination li.active').removeClass('active');
          $this.find('ul.pagination li[data-slide=' + currentSlideHelper.pageId + ']').addClass('active');
        }
      }


      function saveContentState(){
        var currentSlideId = parseInt(returnCurrentSlide().attr('data-slide'), 10);
        elemList[currentSlideId].content = returnCurrentSlide().html();
      }

      function switchCurrentSlide(){
        var slideToSave = returnCurrentSlide();
        locateNextSlide().addClass('current-slide');
        var slideId = parseInt(slideToSave.attr('data-slide'), 10);
        elemList[slideId].content  = slideToSave.html();
        slideToSave.detach();
        if((options.controlsType == 'for-forms') && (returnCurrentSlide().hasClass('last-slide'))){
          $this.find(options.nextClass).detach();
        }
      }

      function gotoHorizontalPrev(gotoSlide){
        gotoSlide = parseInt(gotoSlide, 10);
        if(!isAnimating){
          isAnimating = true;
          returnCurrentSlide().css('float', 'right !important');
          returnSlideContainer().css('width', returnWidth() * 2 + "px !important");                       //resize wrapper to two-slide size
          returnCurrentSlide().before(pasteSlide(elemList[gotoSlide]));              //insert next slide before current one
          currentSlideHelper = elemList[gotoSlide];                       //assign new current_slide
          locateNextSlide().css({'margin-left':parseInt(returnCurrentSlide().css('margin-left'), 10) - returnWidth() + "px", 'float':'left'});
          returnSlideContainer().animate({"marginLeft": parseInt(returnSlideContainer().css('margin-left'), 10) + returnWidth() + "px"}, options.slideSpeed);
          returnSlideContainer().promise().done(function(){     //wait until sliding is finished
            switchCurrentSlide();
            returnCurrentSlide().css('margin-left', '');  //remove margin from the current slide
            returnSlideContainer().css({'width':returnWidth() + "px !important", 'margin-left':''});  //resize wrapper to one-slide size                         
            isAnimating = false;
          });
        }
        updatePagination();
      }

      function gotoHorizontalNext(gotoSlide){
        gotoSlide = parseInt(gotoSlide, 10);
        if(!isAnimating){
          isAnimating = true;
          returnCurrentSlide().css('float', 'left');
          returnSlideContainer().css('width', returnWidth() * 2 + "px");
          returnCurrentSlide().after(pasteSlide(elemList[gotoSlide]));
          currentSlideHelper = elemList[gotoSlide];
          locateNextSlide().css('float', 'right');
          returnSlideContainer().animate({"marginLeft": parseInt(returnSlideContainer().css('margin-left'), 10) - returnWidth() + "px"}, options.slideSpeed);
          returnSlideContainer().promise().done(function(){  //wait until sliding is finished
            switchCurrentSlide();
            returnCurrentSlide().css('margin-left', '');
            returnSlideContainer().css({'width':returnWidth() + "px", 'margin-left': ""});
            isAnimating = false;
          });
        }
        updatePagination();
      }

      function autoplayReset(){
        if(options.autoplay){
          interval = setInterval(function(){gotoHorizontalNext(currentSlideHelper.nextElem);}, options.pauseDuration);
        }else{
          clearInterval(interval);
        }
      }
      autoplayReset();

      $this.find(options.prevClass).click(function(e){
        if(!options.looping && $this.find('li.current-slide').hasClass('first-slide')){
          e.preventDefault();
        }else{
          e.preventDefault();
          gotoHorizontalPrev(currentSlideHelper.prevElem);
        }
        if(options.pauseAfterClick && options.autoplay){
          options.autoplay = false;
          clearInterval(interval);
        }
        if(!options.pauseAfterClick && options.autoplay){
          clearInterval(interval);
          autoplayReset();
        }
      });

      $this.find(options.nextClass).click(function(e){
        if(!options.looping && $this.find('li.current-slide').hasClass('last-slide')){
          e.preventDefault();
        }else{
          e.preventDefault();
          gotoHorizontalNext(currentSlideHelper.nextElem);
        }
        if(options.pauseAfterClick && options.autoplay){
          options.autoplay = false;
          clearInterval(interval);
        }
        if(!options.pauseAfterClick && options.autoplay){
          clearInterval(interval);
          autoplayReset();
        }
      });

      // pagination section
      if(options.pagination){
        var pagination = $this.find('ul.pagination');
        var slide;
        $.each(elemList, function(i){
          if(elemList[i].pageId == parseInt(0, 10)){
            pagination.append("<li data-slide=\"" + elemList[i].pageId + "\" class=\"active\"></li>");
          }else{
            pagination.append("<li data-slide=\"" + elemList[i].pageId + "\"></li>");
          }
        });

        if(options.paginationPositionX == 'center'){
          $this.find('ul.pagination').css('margin-left', returnPaginationMargin() + "px");
        }

        if(options.paginationPositionY == 'bottom'){
          $this.find('ul.pagination').css('margin-top', returnHeight() - parseInt($('ul.pagination').height(), 10) + 'px'); 
        }
        
        $(window).resize(function(){
          returnWidth();
          returnHeight();
          returnCurrentMargin();
          returnButtonWidth();
          returnButtonHeight();
          updateCurrentSlide();
          returnPaginationMargin();
          setControls();
          if(options.paginationPositionX == 'center'){
            $this.find('ul.pagination').css('margin-left', returnPaginationMargin());
          }
        });

        $('ul.pagination li').click(function(){
          if(!$(this).hasClass('active')){
            $(this).removeClass('active');
          }
          if(parseInt($(this).parent().parent(options.mainWrapper).find('ul.slide-container li.current-slide').data('slide'), 10) < parseInt($(this).data('slide'),10)){
            gotoHorizontalNext(parseInt($(this).data('slide'), 10));
          }else if(parseInt($(this).parent().parent(options.mainWrapper).find('ul.slide-container li.current-slide').data('slide'), 10) > parseInt($(this).data('slide'), 10)){
            gotoHorizontalPrev(parseInt($(this).data('slide'), 10));
          }
          $(this).addClass('active');
          if(options.pauseAfterClick && options.autoplay){
            options.autoplay = false;
            clearInterval(interval);
          }
          if(!options.pauseAfterClick && options.autoplay){
            clearInterval(interval);
            autoplayReset();
          }
        });
      }
    });
  };
}(jQuery, false));

/*TODO: update next/prev slide id when clicking on next/prev buttons (not pagination!)*/