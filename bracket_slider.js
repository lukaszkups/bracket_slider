/*
 * jQuery bracket_slider v0.9
 * 
 * Copyright (c) 2013 Lukasz Kups
 * http://webrackets.com
 * http://lukaszkups.pl
 * @ofcapl
 *
 * Licensed under the MIT license: http://www.opensource.org/licenses/mit-license.php
 */

(function($, f){
  if(!$){ return f;}

  $.fn.bslide = function(options){
    options = $.extend({
      sliding_speed : 500,
      slide_orientation: 'horizontal',
      next_class : '.next',
      prev_class : '.prev',
      starter_slide : 0,
      controls_size : 'small',       //huge = 50% of the slide each, small = size defined in css
      pagination : true,
      pagination_position : 'center',
      pagination_width : 0,
      autoplay : false,
      pause_duration: 3000,
      looping : true,
    }, options);
    
    return this.each(function() {  
      var $this = $(this);
      var what_width = $this.width();                                           //width of the slider
      var what_height = $this.height();                                         //height of the slider
      var is_animating = false;                                                 //helper variable to prevent user from fast multi-clicking
      var prev_button_width = $this.find(options.prev_class).width();
      var prev_button_height = $this.find(options.prev_class).height();
      var next_button_width = $this.find(options.next_class).width();
      var next_button_height = $this.find(options.next_class).height();
      var pagination_margin = (what_width - options.pagination_width)/2 + 'px';

      function init_variables(){      //for update purposes when window is resized
        what_width = $this.width();
        what_height = $this.height();
        is_animating = false;                                                 
        prev_button_width = $this.find(options.prev_class).width();
        prev_button_height = $this.find(options.prev_class).height();
        next_button_width = $this.find(options.next_class).width();
        next_button_height = $this.find(options.next_class).height();
        pagination_margin = (what_width - options.pagination_width)/2 + 'px';
      }
      var interval;
      
      $(window).resize(init_variables);
      //looks for current item margin
      function current_margin(){
        return parseInt($(this).css('margin-left'), 10); 
      }

      //finds slide container (ul)
      function slide_container(){
        return $this.find('ul.slide_container');
      }

      //finds current slide(li.current_slide)
      function current_slide(){
        return slide_container().find('li.current_slide');
      }

      //finds next slide (not current)
      function locate_next_slide(){
        return slide_container().find('li.slide').not('.current_slide'); 
      }

      function next_slide(){
        return $(this).next_elem;
      }

      function prev_slide(){
        return $(this).prev_elem;
      }
      
      // next/prev slide controls position, size etc.
      function controls_size(){
        if(options.controls_type=="huge"){
          $this.find(options.prev_class).css('width', (what_width/2) + 'px').css('height', what_height + 'px').css('margin-top', '-' + what_height + 'px');
          $this.find(options.next_class).css('width', (what_width/2) + 'px').css('height', what_height + 'px').css('margin-left', (what_width/2) + 'px').css('margin-top', '-' + what_height + 'px');
        }else if(options.controls_type=="small"){
          $this.find(options.prev_class).css('margin-top', '-' + ((what_height + prev_button_height)/2) + 'px');
          $this.find(options.next_class).css({'margin-top': '-' + ((what_height + next_button_height)/2) + 'px', 'margin-left':(what_width - next_button_width) + 'px'});
        }else if(options.controls_type=="top-right"){
          $this.find(options.prev_class).css({'margin-top': '-' + prev_button_height + 'px', 'margin-left': (what_width - next_button_width*3) + 'px'});
          $this.find(options.next_class).css({'margin-top': '-' + next_button_height + 'px', 'margin-left': (what_width - next_button_width*1.75) + 'px'});
        }
      }
      $(document).ready(controls_size);
      $(window).resize(controls_size);
      
      function slide_container_size(){
        slide_container().css('width', what_width + "px").css('height', what_height + "px");    //add proper width and height for slide wrapper(ul)
      }
      $(document).ready(slide_container_size);
      $(window).resize(slide_container_size);

      $this.find(options.prev_class).addClass('active_button');
      $this.find(options.next_class).addClass('active_button');

      //looks for all elements in slider and takes it to the slides list (setup)
      var elem_list = [];
      $this.find('li.slide').each(function(i){
        var list_obj = {
          content: $(this).html(),
          next_elem: i+1,
          prev_elem: i-1,
          page_id: i,
          class_list: $(this).attr('class')
        };
        elem_list.push(list_obj);
        $(this).detach();
      });
      

      function put_slide_item(which_item){  //when inserting new prev/next slide, add proper widths/heights to it
        return "<li data-slide=\"" + which_item.page_id + "\" class=\"" + which_item.class_list + "\" style=\"width:" +  what_width + "px; height:" + what_height + "px;\">" + which_item.content + "</li>";
      }

      function update_current_slide(){
        $('li.current_slide').css('width', what_width + 'px').css('height', what_height + 'px');
      }
      $(window).resize(update_current_slide);

      elem_list[0].prev_elem = elem_list.length - 1;          //add pointer to the last slide when on first slide prev is clicked
      elem_list[elem_list.length - 1].next_elem = 0;          //add pointer to the first slide when on last slide next is clicked

      var current_slide_helper = elem_list[options.starter_slide];    //set current_slide for the first time to the starter slide no.

      //add first slide on page to the slide wrapper and makes it current for the first time
      slide_container().append(put_slide_item(elem_list[options.starter_slide]));    
      $this.find('li.slide').addClass('current_slide');

      function update_pagination(){
        if(options.pagination){
          $this.find('ul.pagination li.active').removeClass('active');
          $this.find('ul.pagination li[data-slide='+current_slide_helper.page_id+']').addClass('active');
        }
      }
      
      function goto_horizontal_prev(goto_slide){
        if(!is_animating){
          is_animating = true;
          current_slide().css('float', 'right !important');
          slide_container().css('width', what_width * 2 + "px !important");                       //resize wrapper to two-slide size
          current_slide().before(put_slide_item(elem_list[goto_slide]));              //insert next slide before current one
          current_slide_helper = elem_list[goto_slide];                       //assign new current_slide
          locate_next_slide().css('margin-left', parseInt(current_slide().css('margin-left'), 10) - what_width + "px").css('float', 'left');
          slide_container().animate({"marginLeft": parseInt(slide_container().css('margin-left'), 10) + what_width + "px"}, options.sliding_speed);
          slide_container().promise().done(function(){                                            //wait until sliding is finished
            current_slide().remove();                                                             //remove previous slide
            $this.find('li.slide').addClass('current_slide').css('margin-left', '');              //remove margin from the current slide
            slide_container().css('width', what_width + "px !important").css('margin-left', '');  //resize wrapper to one-slide size                         
            is_animating = false;
          });
        }
        update_pagination();
      }
      
      function goto_horizontal_next(goto_slide){
        if(!is_animating){
          is_animating = true;
          current_slide().css('float', 'left');
          slide_container().css('width', what_width * 2 + "px");
          current_slide().after(put_slide_item(elem_list[goto_slide]));
          current_slide_helper = elem_list[goto_slide];
          locate_next_slide().css('float', 'right');
          slide_container().animate({"marginLeft": parseInt(slide_container().css('margin-left'), 10) - what_width + "px"}, options.sliding_speed);
          slide_container().promise().done(function(){  //wait until sliding is finished
            current_slide().remove();
            $this.find('li.slide').addClass('current_slide').css('margin-left', '');  
            slide_container().css('width', what_width + "px").css('margin-left', "");
            is_animating = false;
          });
        }
        update_pagination();        
      }
      
       // autoplay section
      function autoplay_reset(){
        if(options.autoplay){
          interval = setInterval(function(){goto_horizontal_next(current_slide_helper.next_elem);}, options.pause_duration);
        }
      }
      

      $this.find(options.prev_class).click(function(e){
        if(!options.looping && $this.find('li.current_slide').hasClass('first-slide')){
            e.preventDefault();
        }else{
            e.preventDefault();
            goto_horizontal_prev(current_slide_helper.prev_elem);
            clearInterval(interval);
            setTimeout(autoplay_reset, options.pause_duration);
        }
      });

      $(document).keypress(function(event){
        if(event.keyCode === 37){
          if(!options.looping && $this.find('li.current_slide').hasClass('first-slide')){
            event.preventDefault();
          }else{
              goto_horizontal_prev(current_slide_helper.prev_elem);
              clearInterval(interval);
              setTimeout(autoplay_reset, options.pause_duration);
          }
        }
      });

      $this.find(options.next_class).click(function(e){
        if(!options.looping && $this.find('li.current_slide').hasClass('last-slide')){
            e.preventDefault();
        }else{
            e.preventDefault();
            goto_horizontal_next(current_slide_helper.next_elem);
            clearInterval(interval);
            setTimeout(autoplay_reset, options.pause_duration);
        }
      });

      $(document).keypress(function(event){
        if(event.keyCode === 39){
          if(!options.looping && $this.find('li.current_slide').hasClass('last-slide')){
            event.preventDefault();
          }else{
              goto_horizontal_next(current_slide_helper.next_elem);
              clearInterval(interval);
              setTimeout(autoplay_reset, options.pause_duration);
          }
        }
      });

      // pagination section
      if(options.pagination){
        var pagination = $this.find('ul.pagination');
        var slide;
        $.each(elem_list, function(i){
          if(elem_list[i].page_id == options.starter_slide){
            pagination.append("<li data-slide=\"" + elem_list[i].page_id + "\" class=\"active\"></li>");
          }else{
            pagination.append("<li data-slide=\"" + elem_list[i].page_id + "\"></li>");
          }
        });

        if(options.pagination_position == 'center'){
          $this.find('ul.pagination').css('margin-left', pagination_margin);
        }
        
        $(window).resize(function(){
          if(options.pagination_position == 'center'){
            $this.find('ul.pagination').css('margin-left', pagination_margin);
          }
        });

        $('ul.pagination li').click(function(){
          if(!$(this).hasClass('active')){
            $('ul.pagination li.active').removeClass('active');
          }
          if($('ul.slide_container li.current_slide').data('slide') < $(this).data('slide')){
            goto_horizontal_next($(this).data('slide'));
          }else if($('ul.slide_container li.current_slide').data('slide') > $(this).data('slide')){
            goto_horizontal_prev($(this).data('slide'));
          }
          $(this).addClass('active');
          clearInterval(interval);
          setTimeout(autoplay_reset, options.pause_duration);
        });
      }
      
      autoplay_reset();
     
      
    });
  };
}(jQuery, false));